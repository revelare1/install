import pytest
from httpx import AsyncClient
import pytest_asyncio
import os
from dotenv import load_dotenv


load_dotenv()


if not os.getenv("LOCAL_URL"):
    os.environ["LOCAL_URL"] = "127.0.0.1"
local_url = os.environ["LOCAL_URL"]


@pytest.fixture(name="client")
async def client_fixture():
    async with AsyncClient() as client:
        yield client


async def test_access_frontend(client: AsyncClient):
    response = await client.get(f"http://{local_url}:8002/")
    assert response.status_code == 200


async def test_access_backend(client: AsyncClient):
    response = await client.get(f"http://{local_url}:8000/healthcheck")
    assert response.status_code == 200
    data = response.json()
    assert data['status'] == 'ok'

"""
async def test_summarize(client: AsyncClient):
    response = await client.post(f"http://{local_url}:8000/summarize",
                                 json={'title': 'The great divergence',
                                       'url': 'https://thenextrecession.wordpress.com/2023/07/10/the-great-divergence/',
                                       'content': '',
                                       'tags': ['Test tag 1', 'Test tag 2']}, timeout=None)

    assert response.status_code == 200
    data = response.json()
    assert data['title']
    assert data['url']
    assert data['summary']
    assert data['tags']
"""
