# Revelare
An application to enable greater information intake through LLM-based summarization of texts from the internet.

## Installation and setup

0. Install Docker (and docker-compose) *[or use podman/podman-compose, if you want to]*.
1. Clone this repo.
2. Edit `.env` to add your environment variables. Add your chosen backend for language processing after `LLM_SOURCE=`. Available alternatives are `local` and `openai` at the moment. If you choose `openai`, you also need to enter your API key after `OPENAPI_API_KEY=`.
3. Run `docker compose -f compose.yaml up -d`.

## Usage and examples

### Summarization
You can use Revelare to summarize texts in two ways.

**Supplying the text**

If you send along a complete text to the API, the summarizer will return a summary for the supplied text.

**Supplying a URL**

If you only send a url and no text to the API, Revelare attempts to extract the text (using txtai's Textractor or LangChain's various scrapers) before it's sent to the summarizer.

### Search (not implemented)
You can use Revelare to search for specific topics in the information you have gathered. This works using an LLM for semantic search in the summaries stored in your database. The search returns the summaries whose topics are semantically closest to your query.

### Sources

A **Source** is a way of generating new summaries. An example of a source is a specific RSS feed or a specific user's Twitter feed. As a user of Revelare, you can create a source based on the RSS Feed for the BBC. When you then refresh the source, summaries will be generated for any new items in the feed.

### Briefs

A **Brief** is a text based on existing summaries from a specified date range. A brief consists of a set of topics important in the summaries, with summarizations of these topics as they appeared in the relevant summaries. The user can supply topics that they want to have included in the brief.

## Support

## Roadmap

### v0.1
- [x] Summarization using the web API
- [x] Frontend for interacting with the web API 
- [x] Default large language model chosen and included

### v0.2
- [x] Rewrite the pipeline as a multi-project pipeline (revelare1/llm) --> (revelare1/backend) --> (revelare1/install), where the first steps are only triggered if they have commits since last run
- [ ] (Semantic) search using the web API
- [x] Option to use OpenAI or local LLM
- [?] Automatic tagging of summaries upon summarization (https://python.langchain.com/v0.1/docs/use_cases/tagging/)
- [ ] Implement a loader function using various scrapers/loaders from LangChain (https://python.langchain.com/v0.2/docs/concepts/#document-loaders)
- [ ] Simplify installation for Windows-users?
- [x] Transcription of video/audio with timestamped, summarized and tagged parts (not tested thoroughly)

### v0.3
- [ ] Rewrite the LangChain implementation according to the migration guide at https://python.langchain.com/v0.2/docs/versions/migrating_chains/
- [x] Automatic generation of briefs based on sources/topics
- [x] Framework for subscribing to sources in general
- [ ] Test various models for local processing
- [ ] Config options for running locally on your own machine or on a local server (auth?)
- [ ] Possibility of interacting with your summaries using a chat interface
- [ ] RAG interface for searching your stored documents (https://neuml.hashnode.dev/build-rag-pipelines-with-txtai)
- [ ] Implement level 4 (BRV) and level 5 (agent) of the summarization methods here: https://github.com/gkamradt/langchain-tutorials/blob/main/data_generation/5%20Levels%20Of%20Summarization%20-%20Novice%20To%20Expert.ipynb

### v0.4
- [ ] Store the content of texts somehow?
- [ ] Refactor source implementation using some form of subclassing
- [ ] Implement auth stack using Ory
- [ ] Finetuning of local llm using rating of summaries
- [ ] Building RAG pipelines where agents can retrieve information from networked sources 
- [ ] Consider changing to LangServe (https://python.langchain.com/v0.2/docs/langserve/#setup)

#### Maybe stuff to look at?

https://neo4j.com/blog/graphrag-manifesto/?mkt_tok=NzEwLVJSQy0zMzUAAAGVYDnHOs0D-7HY92bNKlPAPKFEi_XPaMBKC8SjIgS1TrPwuNaFnhUyib6hL6celjsBJONbnCA6hSkpg2w4ldjdaEpgwEGPKDsTfZUr3pQOFDd8QdY

## Documentation

The basic structure of Revelare is this:

![Basic structure of Summarizer](docs/revelare.png)

Revelare's frontend is built using [Skeleton](https://skeleton.dev), compiled and served locally using a [Caddy](https://caddyserver.com/) container. You can access the frontend at https://127.0.0.1:8002. Everything you see there is either gathered from the local Caddy container or from Revelare's backend.

Revelare's backend uses a [FastAPI](https://fastapi.tiangolo.com/) container to serve an API for interacting with your chosen LLM interface. That interaction is done using [LangChain](https://www.langchain.com/), regardless of your chosen LLM. The backend uses [SQLModel](https://sqlmodel.tiangolo.com/) to communicate with the [PostgresQL](https://www.postgresql.org/) database container.

If you want to peruse the web API directly, go to http://127.0.0.1:8000/docs. An interface for the local llm (if run) is at http://127.0.0.1:8080.

In the ontology of Revelare, there are **texts**, which are basically items of interest with a URL (whether local or on the internet), title, content and timestamp. These texts can be fed directly by the user or generated by a **source**. Sources are user-defined self-generating information deposits, like a user's Twitter feed or an RSS feed. Texts are turned into **summaries**, that are stored in the database *without the text's content, only its summary*. From these summaries, a user can then generate a **brief** containing summaries of the most important topics in the summaries within a specific date range.

## License
AGPLv3
